<?php
header('content-type:text/html;charset=utf-8');
//定义允许上传的类型
$types=array("image/jpge","image/jpg","image/png","image/gif");
$folder="images";
//判断是否存在目录
if(!is_dir($folder)){
	mkdir($folder);
}
if(isset($_FILES['myfile'])){
	if(in_array($_FILES['myfile']['type'],$types)){//如果是允许的文件类型
		$thefile=$folder."/".time().rand(1,9999).$_FILES['myfile']['name'];//定义上传路径
		if(!move_uploaded_file($_FILES['myfile']['tmp_name'],$thefile)){//上传文件
			echo "上传文件失败";
		}else{
			?>
			<script type="text/javascript" src="js/function.js"></script>
			<body onload="doneloading(parent,'<?php echo $thefile;?>')">
				<img src="<?php echo $thefile;?>">
			</body>


			<?php
		}
	}else{
		echo "文件类型不合法";
		exit();
	}
}